<?php

namespace App\Http\Controllers;

use App\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CalculatorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::where('rate_to_shekel', '>', 0)->get();
        return view('calculator.index', compact('rates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function calculate(Request $request)
    {
        $sellCurreny = Rate::findOrFail($request->sell_currency);
        $buyCurreny = Rate::findOrFail($request->buy_currency);
        $buy_amount = $sellCurreny->rate_to_shekel * $request->sell_amount / $buyCurreny->rate_to_shekel;

        Session::flash('buy_amount', $buy_amount);
        Session::flash('sell_currency', $request->sell_currency);
        Session::flash('sell_amount', $request->sell_amount);
        Session::flash('buy_currency', $request->buy_currency);
        return redirect('calculator');
    }
}
