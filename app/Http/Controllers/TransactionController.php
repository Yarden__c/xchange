<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Transaction;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $rates = Rate::all();
        $customers = Customer::all();
        if (auth()->user()->isAdmin()) {
            $transactions = Transaction::paginate(10);
        } else {
            $transactions = Transaction::where('user_id', auth()->user()->id)->paginate(10);
        }
        return view('transactions.index', compact('transactions', 'customers', 'rates','users'));
    }

    public function indexsorted($by)
    {
        $users = User::all();
        $rates = Rate::all();
        $customers = Customer::all();
        $transactions = Transaction::query()->orderBy($by)->paginate(10);
        return view('transactions.index', compact('transactions', 'customers', 'rates','users'));
    }
    public function indexfiltered($by, $what)
    {
        $users = User::all();
        $rates = Rate::all();
        $customers = Customer::all();
        $transactions = Transaction::query()->where($by, $what)->paginate(10);
        return view('transactions.index', compact('transactions', 'customers', 'rates','users'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $customers = Customer::all();
        $rates = Rate::all();
        return view('transactions.create', compact('users', 'customers', 'rates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFromCalc(Request $request)
    {
        $sellCurreny = Rate::findOrFail($request->sell_currency);
        $buyCurreny = Rate::findOrFail($request->buy_currency);
        $buy_amount = $sellCurreny->rate_to_shekel * $request->sell_amount / $buyCurreny->rate_to_shekel;

        Session::flash('buy_amount', $buy_amount);
        Session::flash('sell_currency', $request->sell_currency);
        Session::flash('sell_amount', $request->sell_amount);
        Session::flash('buy_currency', $request->buy_currency);
        $users = User::all();
        $customers = Customer::all();
        $rates = Rate::all();
        return view('transactions.create', compact('users', 'customers', 'rates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = new Transaction();
        if(auth()->user()->isAdmin()){
            $transaction->user_id = $request->user_id;
        } else {
            $transaction->user_id = auth()->user()->id;
        }
        $transaction->customer_id = $request->customer_id;
        $transaction->sold_id = $request->sold_id;
        $transaction->sold_amount = $request->sold_amount;
        $transaction->bought_id = $request->bought_id;
        $transaction->bought_amount = $request->bought_amount;
        $transaction->commission_percentage = $request->commission_percentage;
        $transaction->save();
        return redirect('transactions');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::findOrFail($id);
        return view('transactions.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::findOrFail($id);
        $users = User::all();
        $customers = Customer::all();
        $rates = Rate::all();
        return view('transactions.edit', compact('transaction', 'users', 'customers', 'rates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->update($request->all());
        return redirect('transactions');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->delete();
        return redirect('transactions');
    }
    public function bulkdelete(Request $req)
    {
        $id = $req->id;
        foreach ($id as $transaction) {
            $transaction = Transaction::find($transaction);
            $transaction->delete();
        }
        return redirect()->back()->with('message','Successfully Delete Your Multiple Selected Records.');;
    }
}
