<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'symbol', 'rate_to_shekel'
    ];

    public function transactionsSold(){
        return $this->hasMany('App\Transaction','sold_id');
    }

    public function transactionsBought(){
        return $this->hasMany('App\Transaction','bought_id');
    }
}
