@extends('layouts.app')

@section('title', 'Transactions')

@section('content')
    @if(Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <div class="table-header">
    <h1>List of transactions</h1>
    
    <a class="btn btn-primary" href="{{url('/transactions/create')}}"> Add new transaction</a>
    </div>
    <div class="table-header" >    
    <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                Filter By customer
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($customers as $customer)
                    <a class="dropdown-item" href="{{route('transactions.filtered',['customer_id', $customer->id])}}">{{$customer->name}}</a>
                @endforeach
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filter By Sold
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($rates as $rate)
                    <a class="dropdown-item" href="{{route('transactions.filtered',['sold_id', $rate->id])}}">{{$rate->symbol}}</a>
                @endforeach
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filter By Bought
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($rates as $rate)
                    <a class="dropdown-item" href="{{route('transactions.filtered',['bought_id', $rate->id])}}">{{$rate->symbol}}</a>
                @endforeach
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filter By User
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($users as $user)
                    <a class="dropdown-item" href="{{route('transactions.filtered',['user_id', $user->id])}}">{{$user->name}}</a>
                @endforeach
            </div>
        </div>
        <!-- <a class="btn btn-primary" href="{{url('/transactions/filtered/id/2')}}"> Filter by user</a> -->
    </div>
    
    <form method="post" action="{{url('transactions/bulkdelete')}}">
        @csrf
    <table class="table">
        <tr>
        <th><input type="checkbox" id="checkAll"> Select All</th>
            <th><a href = "{{url('/transactions/sorted/id')}}">id</a></th>
            @if(auth()->user()->role === 'admin')
                <th><a href = "{{url('/transactions/sorted/user_id')}}">User</a></th>
            @endif
            <th><a href = "{{url('/transactions/sorted/customer_id')}}">Customer</a></th>
               <th><a href = "{{url('/transactions/sorted/sold_id')}}">Currency Sold</a></th>
               <th><a href = "{{url('/transactions/sorted/sold_amount')}}">Sold Amount</a></th>
               <th><a href = "{{url('/transactions/sorted/bought_id')}}">Currency Bought</a></th>
               <th><a href = "{{url('/transactions/sorted/bought_amount')}}">Bought Amount</a></th>
               <th><a href = "{{url('/transactions/sorted/commission_percentage')}}">Commission Percentage</a></th>
               <th><a href = "{{url('/transactions/sorted/created_at')}}">Created At</a></th>
               <th><a href = "{{url('/transactions/sorted/updated_at')}}">Updated At</a></th>
        </tr>
        <!-- the table data -->
        @foreach($transactions as $transaction)
            <tr>
                <td><input name='id[]' type="checkbox" id="checkItem" value="{{$transaction->id}}">
                <td>{{$transaction->id}}</td>
                    @if(auth()->user()->role === 'admin')
                        <td>{{$transaction->user->name}}</td>
                    @endif
                <td>{{$transaction->customer->name}}</td>
                <td>{{$transaction->sold->symbol}}</td>
                <td>{{$transaction->sold_amount}}</td>
                <td>{{$transaction->bought->symbol}}</td>
                <td>{{$transaction->bought_amount}}</td>
                <td>{{$transaction->commission_percentage}}</td>
                <td>{{$transaction->created_at}}</td>
                <td>{{$transaction->updated_at}}</td>
                <td>
                    <a href="{{route('transactions.edit',$transaction->id)}}">Edit</a>
                </td>
                <td>
                    <a href="{{route('transaction.delete',$transaction->id)}}">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
    <input class="btn btn-primary" type="submit" name="submit" value="Delete Selected Transactions"/>
</form>
    {{$transactions->links()}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $("#checkAll").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
        });
</script>
@endsection