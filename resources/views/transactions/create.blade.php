@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Transaction</div>

                    <div class="card-body">
                        <form method="POST" action="{{action('TransactionController@store')}}">
                            @csrf
                            @if(auth()->user()->role === 'admin')
                                <div class="form-group row">
                                    <label for="user_id" class="col-md-4 col-form-label text-md-right">User</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="user_id">
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">
                                                    {{ $user->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="customer_id" class="col-md-4 col-form-label text-md-right">Customer</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="customer_id">
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->id }}">
                                                {{ $customer->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sold_id" class="col-md-4 col-form-label text-md-right">Sold Currency</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="sold_id">
                                        @foreach ($rates as $rate)
                                            @if($rate->id == Session::get('sell_currency'))
                                                <option value="{{ $rate->id }}" selected="selected">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @else
                                                <option value="{{ $rate->id }}">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sold_amount" class="col-md-4 col-form-label text-md-right">{{ __('Sold Amount') }}</label>

                                <div class="col-md-6">
                                    <input id="sold_amount" type="text" value="{{Session::get('sell_amount')}}" class="form-control @error('sold_amount') is-invalid @enderror" name="sold_amount" required autocomplete="sold_amount" autofocus>

                                    @error('sold_amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="bought_id" class="col-md-4 col-form-label text-md-right">Bought Currency</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="bought_id">
                                        @foreach ($rates as $rate)
                                            @if($rate->id == Session::get('buy_currency'))
                                                <option value="{{ $rate->id }}" selected="selected">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @else
                                                <option value="{{ $rate->id }}">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bought_amount" class="col-md-4 col-form-label text-md-right">{{ __('Bought Amount') }}</label>

                                <div class="col-md-6">
                                    <input id="bought_amount" type="text" value="{{Session::get('buy_amount')}}" class="form-control @error('bought_amount') is-invalid @enderror" name="bought_amount" required autocomplete="bought_amount" autofocus>

                                    @error('bought_amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="commission_percentage" class="col-md-4 col-form-label text-md-right">{{ __('Commission Percentage') }}</label>

                                <div class="col-md-6">
                                    <input id="commission_percentage" type="text" class="form-control @error('commission_percentage') is-invalid @enderror" name="commission_percentage" required autocomplete="commission_percentage" autofocus>

                                    @error('commission_percentage')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection