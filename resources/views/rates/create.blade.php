@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create rate</div>

                    <div class="card-body">
                        <form method="POST" action="{{action('RateController@store')}}">
                            @csrf
                            <div class="form-group row">
                                <label for="symbol" class="col-md-4 col-form-label text-md-right">{{ __('Symbol') }}</label>

                                <div class="col-md-6">
                                    <input id="symbol" type="text" class="form-control @error('symbol') is-invalid @enderror" name="symbol" required autocomplete="symbol" autofocus>

                                    @error('symbol')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rate_to_shekel" class="col-md-4 col-form-label text-md-right">{{ __('Rate To Shekel') }}</label>

                                <div class="col-md-6">
                                    <input id="rate_to_shekel" type="text" class="form-control @error('rate_to_shekel') is-invalid @enderror" name="rate_to_shekel" required autocomplete="rate_to_shekel">

                                    @error('rate_to_shekel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection