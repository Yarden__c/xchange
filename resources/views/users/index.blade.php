@extends('layouts.app')

@section('title', 'Users')

@section('content')
    @if(Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <div class="table-header">
    <h1>List of users</h1>
    <a class="btn btn-primary" href="{{url('/users/create')}}"> Add new user</a>
    </div>
    <table class="table">
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>Role</th>
            <th>Phone</th>
            <th>Created</th>
            <th>Updated</th>
        </tr>
        <!-- the table data -->
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->address}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle"
                                type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            {{$user->role}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               href="{{route('users.changerole',[$user->id, 'admin'])}}">Admin</a>
                            <a class="dropdown-item"
                               href="{{route('users.changerole',[$user->id, 'user'])}}">User</a>
                        </div>
                    </div>
                </td>
                <td>{{$user->phone}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td>
                    <a href="{{route('users.edit',$user->id)}}">Edit</a>
                </td>
                <td>
                    <a href="{{route('user.delete',$user->id)}}">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{$users->links()}}
@endsection