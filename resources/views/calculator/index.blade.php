@extends('layouts.app')

@section('title', 'Customers')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Rate calculator</div>

                    <div class="card-body">
                        <form method="POST" action="{{action('CalculatorController@calculate')}}">
                            @csrf
                            <div class="form-group row">
                                <label for="sell_currency" class="col-md-4 col-form-label text-md-right">Sell Currency</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="sell_currency">
                                        @foreach ($rates as $rate)
                                            @if($rate->id == Session::get('sell_currency'))
                                                <option value="{{ $rate->id }}" selected="selected">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @else
                                                <option value="{{ $rate->id }}">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @endif

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sell_amount" class="col-md-4 col-form-label text-md-right">Sell Amount</label>
                                <div class="col-md-6">
                                    <input id="sell_amount" type="text" class="form-control" name="sell_amount" value="{{Session::get('sell_amount')}}" required autocomplete="sell_amount" autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="buy_currency" class="col-md-4 col-form-label text-md-right">Buy Currency</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="buy_currency">
                                        @foreach ($rates as $rate)
                                            @if($rate->id == Session::get('buy_currency'))
                                                <option value="{{ $rate->id }}" selected="selected">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @else
                                                <option value="{{ $rate->id }}">
                                                    {{ $rate->symbol }}
                                                </option>
                                            @endif

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="buy_amount" class="col-md-4 col-form-label text-md-right">Buy Amount</label>
                                <div class="col-md-6">
                                    <input name="buy_amount" class="form-control" value="{{Session::get('buy_amount')}}" disabled>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Calculate
                                    </button>
                                    <input type="submit" class="btn btn-primary" value="Add new transaction" formaction="{{url('/transactions/createfromcalc')}}">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection