<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('transactions');
});
Route::get('/home', function () {
    return redirect('transactions');
});

Auth::routes(['register' => false]);
Route::resource('users', 'UserController')->middleware('auth');
Route::get('users/changerole/{uid}/{role}', 'UserController@changeRole')->name('users.changerole');
Route::get('users/{id}/delete/', 'UserController@destroy')->name('user.delete')->middleware('auth');

Route::get('rates/updateall', 'RateController@updateAll')->name('rate.updateall')->middleware('auth');
Route::resource('rates', 'RateController')->middleware('auth');
Route::get('rates/{id}/delete/', 'RateController@destroy')->name('rate.delete')->middleware('auth');

Route::get('customers/sorted/{by}', 'CustomerController@indexsorted')->name('customers.sorted')->middleware('auth');
Route::resource('customers', 'CustomerController')->middleware('auth');
Route::get('customers/{id}/delete/', 'CustomerController@destroy')->name('customer.delete')->middleware('auth');

Route::get('transactions/sorted/{by}', 'TransactionController@indexsorted')->name('transactions.sorted')->middleware('auth');
Route::get('transactions/filtered/{by}/{what}', 'TransactionController@indexfiltered')->name('transactions.filtered')->middleware('auth');
Route::post('transactions/createfromcalc/', 'TransactionController@createFromCalc')->name('transaction.createfromcalc')->middleware('auth');
Route::resource('transactions', 'TransactionController')->middleware('auth');
Route::get('transactions/{id}/delete/', 'TransactionController@destroy')->name('transaction.delete')->middleware('auth');
Route::post('transactions/bulkdelete', 'TransactionController@bulkdelete')->middleware('auth');

Route::get('calculator', 'CalculatorController@index')->name('calculator.index')->middleware('auth');
Route::post('calculator', 'CalculatorController@calculate')->middleware('auth');
